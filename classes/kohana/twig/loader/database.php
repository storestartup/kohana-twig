<?php

class Kohana_Twig_Loader_Database implements Twig_LoaderInterface
{

    /**
     * Gets the cache key to use for the cache for a given template name.
     *
     * @param  string $name string The name of the template to load
     *
     * @return string The cache key
     */
    public function getCacheKey($name)
    {
        return $name;
    }

    /**
     * Gets the source code of a template, given its name.
     *
     * @param  string $name string The name of the template to load
     *
     * @return string The template source code
     */
    public function getSource($name)
    {
        // query database
        $result = DB::select('source')->from('templates')->where('name','=',$name)->execute();
        
        // throw exception if no rows found
        if ($result->count() < 1) throw new Twig_Error_Loader(sprintf('Template [%s] was not found',$name));
        
        // get and return template source
        return $result->get('source');
    }

    /**
     * Returns true if the template is still fresh.
     *
     * @param string    $name The template name
     * @param timestamp $time The last modification time of the cached template
     */
    public function isFresh($name, $time)
    {
        // load last modified time from db
        $last_modified = DB::select('last_modified')->from('templates')->where('name','=',$name)
                ->execute()->get('last_modified', false);
        
        if ($last_modified === false) {
            return false;
        }

        return strtotime($last_modified) <= $time;
    }

}
