<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Twig
{

    /**
     * @var  Twig_Environment  twig environment for load templates by name
     */
    public static $instance;
    
    /**
     *
     * @var Twig_Environment the twig environment for rendering template strings
     */
    public static $string_instance;

    /**
     * @var  object  Kohana_Twig configuration (Kohana_Config object)
     */
    public static $config;

    public static function instance()
    {
        if (!Kohana_Twig::$instance)
        {
            // Load Twig configuration
            Kohana_Twig::$config = Kohana::$config->load('twig');

            // Create the the twig template loaders
            // first create the database loader
            $db_loader = new Kohana_Twig_Loader_Database();
            
            // then create filesystem loader
            // build an array of possible directories for twig, using kohana cascading file system
            $templates_path = Kohana_Twig::$config->get('templates_directory');
            $template_directories = array();
            // check if twig templates dir exists in app
            if (is_dir(APPPATH.$templates_path)) $template_directories[]=APPPATH.$templates_path;
            // iterate through module directories
            foreach (Kohana::modules() as $module_path)
            {
                // construct twig path inside module
                $temp_path = $module_path . $templates_path;
                // make sure it exists before adding to twig loader directories
                if (is_dir($temp_path)) $template_directories[]=$temp_path;
            }
            $fs_loader = new Twig_Loader_Filesystem($template_directories);
            
            // create the chain loader to use both db and file system loaders, trying the db first
            $loader = new Twig_Loader_Chain(array($db_loader, $fs_loader));

            // Set up Twig environment
            Kohana_Twig::$instance = new Twig_Environment($loader, Kohana_Twig::$config->get('environment'));

            // add extensions
            foreach (Kohana_Twig::$config->get('extensions') as $extension)
            {
                // Load extensions
                Kohana_Twig::$instance->addExtension(new $extension);
            }
        }

        return Kohana_Twig::$instance;
    }
    
    /**
     *  Renders twig template source from a string
     * 
     * @param string $source the twig template source
     * @param array $context
     * @return string rendered template
     */
    public static function render_template($source,$context=array())
    {
        if (!Kohana_Twig::$string_instance)
        {
            // Load Twig configuration
            Kohana_Twig::$config = Kohana::$config->load('twig');

            // Set up Twig environment, with a string loader
            Kohana_Twig::$string_instance = new Twig_Environment(new Twig_Loader_String, Kohana_Twig::$config->get('environment'));

            // add extensions
            foreach (Kohana_Twig::$config->get('extensions') as $extension)
            {
                // Load extensions
                Kohana_Twig::$string_instance->addExtension(new $extension);
            }
        }
        
        return Kohana_Twig::$string_instance->render($source,$context);
    }

    final private function __construct()
    {
        // This is a static class
    }

}